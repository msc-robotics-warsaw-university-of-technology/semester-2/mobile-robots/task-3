function [forwBackVel, leftRightVel, rotVel, finish] = ...
    solution3(pts, contacts, position, orientation, varargin)
% The control loop callback function - the solution for Task 3

    % get the parameters
    if length(varargin) ~= 3
         error(['Wrong number of additional arguments:' ...
             ' %d\n'], length(varargin));
    end
    param_diameter = varargin{1};
    param_length = varargin{2};
    param_angle = varargin{3};
    
    % declare the persistent variable that keeps 
    % the state of the Finite
    % State Machine (FSM)
    persistent state;
    if isempty(state)
        % the initial state of the FSM is 'init'
        state = 'init';
    end

    % initialize the robot control variables 
    % (returned by this function)
    finish = false;
    forwBackVel = 0;
    leftRightVel = 0;
    rotVel = 0;

    % TODO: manage the states of FSM
    u_max = 5;
    u_min = -5;
    gain_P = 100;
    tan_max_vel = 5;
    persistent initial_position;
    persistent second_position;
    persistent dest_position1;
    persistent dest_position2;
    persistent center1;
    persistent center2;
    if strcmp(state, 'init')
        state = 'move_straight_1';
        fprintf('changing FSM state to %s\n', state);
        initial_position = position(1:2);
        dest_position1 = initial_position + ...
            [-(param_length-param_diameter)*cos(param_angle)...  
            (param_length-param_diameter)*sin(param_angle)];
    elseif strcmp(state, 'move_straight_1')
        vel = move_straight(dest_position1,position, ...
                       u_max,u_min,gain_P,orientation);
        forwBackVel = vel(1);
        leftRightVel = vel(2);
        rotVel = 0;
        if (abs(dest_position1(1)-position(1))<0.005 &&  ...
                abs(dest_position1(2)-position(2))<0.005)
            state = 'move_half_circle_1';
            fprintf('changing FSM state to %s\n', state);
            r=param_diameter/2;
            center1 = position(1:2) + ...
                [(r*cos((pi/2)-param_angle)), ...
                (r*sin((pi/2)-param_angle))];
            second_position=position(1:2);
        end
    elseif strcmp(state, 'move_half_circle_1')
        vel = move_circle(center1,position,u_max,u_min, ...
                gain_P,orientation,tan_max_vel,param_diameter);
        forwBackVel = vel(2);
        leftRightVel = vel(1);
        rotVel = 0;
        a = center1-second_position;
        b = center1-position(1:2);
        theta=acos((dot(a,b))/(norm(a)*norm(b)));
        if theta>3.13
            state = 'move_straight_2';
            fprintf('changing FSM state to %s\n', state);
            dest_position2 = position(1:2) + ...
                    [((param_length-param_diameter) ...
                    *cos((2*pi)-param_angle)), ...
                    ((param_length-param_diameter)...
                    *sin((2*pi)-param_angle))];
        end
    elseif strcmp(state, 'move_straight_2')
        vel = move_straight(dest_position2,position,u_max, ...
                            u_min,gain_P,orientation);
        forwBackVel = vel(1);
        leftRightVel = vel(2);
        rotVel = 0;
        if (abs(dest_position2(1)-position(1))<0.005 &&  ...
                abs(dest_position2(2) - position(2)) < 0.005)
            state = 'move_half_circle_2';
            fprintf('changing FSM state to %s\n', state);
            r=param_diameter/2;
            center2 = position(1:2) - ...
                        [(r*cos((pi/2)-param_angle)), ...
                        (r*sin((pi/2)-param_angle))];
            second_position=position(1:2);
        end
    elseif strcmp(state, 'move_half_circle_2')
        vel = move_circle(center2,position,u_max,u_min, ...
                gain_P,orientation,tan_max_vel,param_diameter);
        forwBackVel = vel(2);
        leftRightVel = vel(1);
        a = center2-second_position;
        b = center2-position(1:2);
        theta=acos((dot(a,b))/(norm(a)*norm(b)));
        if theta>3.13
            state = 'move_straight_1';
            fprintf('changing FSM state to %s\n', state);
        end
    elseif strcmp(state, 'finish')
        finish = true;
        disp('finished');
    else
        error('Unknown state %s.\n', state);
    end
end

function R = Rotation_matrix(theta)
    R = [cos(theta), -sin(theta); 
        sin(theta), cos(theta)];
end

function vel_local = move_straight(dest_position,position, ...
                u_max, u_min,gain_P,orientation)
    position_diff=[(dest_position(1) - position(1));
                    (dest_position(2) - position(2))];
    distance=norm(position_diff);
    v=position_diff./distance;
    u_pos = gain_P * distance;
    if u_pos > u_max 
        u_pos = u_max;
    elseif u_pos < u_min
        u_pos = u_min;
    end
    v=v*u_pos;
    Vel_glob = [v(2); v(1); orientation(3)];
    R=[cos(orientation(3)) -sin(orientation(3)) 0; 
        sin(orientation(3)) cos(orientation(3)) 0; 
        0 0 1];
    vel_local = R*Vel_glob;
end

function vel_local = move_circle(center1,position,u_max, ...
                            u_min,gain_P, orientation, ...
                            tan_max_vel,param_diameter)
    % calculate the differnce between center 
    % and current Position of the robot
    diff_from_center = position(1:2)' - center1';

    %calculate the distance between the center 
    % and current Position of the robot
    new_Radius = norm(diff_from_center);

    %calculate the radial direction
    %for a unit vector
    rad_dir = diff_from_center / new_Radius;

    %calculate the tangengial direction perpendicular
    %to radial direction
    tan_dir = Rotation_matrix(-pi/2)*rad_dir;

    %Add proportional regulators with limited output
    radius_err =  (param_diameter/2)- new_Radius;
    u_r = gain_P * radius_err;
    if u_r > 2*u_max
        u_r = 2*u_max;
    elseif u_r < 2*u_min
        u_r = 2*u_min;
    end

    Vel_glob = rad_dir * u_r + tan_dir * tan_max_vel;
    vel_local = Rotation_matrix(orientation(3))*Vel_glob;
end